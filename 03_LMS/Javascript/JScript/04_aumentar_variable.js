function AumentarVariables() {

  //Manejo de la variable precio
  var numero,
      valor,
      resultado;

    resultado = document.getElementById("resultadoAumento");
    valor = 4;
    numero = valor++;
    resultado.innerHTML +=
          "<br> Con valor++  las variables valen: " +
          "valor: " + valor + " numero:" + numero;

    valor = 4;
    numero = ++valor;
    resultado.innerHTML +=
          "<br> Con ++valor  las variables valen: " +
          "valor: " + valor + " numero:" + numero;
}
