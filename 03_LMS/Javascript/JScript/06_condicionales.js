function CalcularDiaMes()
{
	var mes,
			diasMes,
			resultado;

	mes = parseInt(document.getElementById('numeroMes').value);

	/*switch (mes) {
		case 1:diasMes = 31;
			break;
		case 2:diasMes = 28;
			break;
		case 3:diasMes = 31;
			break;
		case 4:diasMes = 30;
			break;
		case 5:diasMes = 31;
			break;
		case 6:diasMes = 30;
			break;
	  case 7:diasMes = 31;
			break;
		case 8:diasMes = 31;
			break;
		case 9:diasMes = 30;
			break;
		case 10:diasMes = 31;
			break;
		case 11:diasMes = 30;
			break;
		case 12:diasMes = 31;
			break;
		default:diasMes = 99;
			break;
	}*/

	switch (mes) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:diasMes = 31;
						break;
		case 4:
		case 6:
 		case 9:
		case 11:diasMes = 30;
				 		break;
		case 2: diasMes = 28;
						break;

		default:diasMes = 99;
						break;
	}
	
	resultado = document.getElementById('resultado');

	if (diasMes !=99)
		resultado.innerHTML +=
			"El mes " + mes + " tiene " + diasMes + " dias.";
	else {
		resultado.innerHTML +=
			"El mes " + mes + " no es correcto (Prueba 1-12)";
	 }
}
