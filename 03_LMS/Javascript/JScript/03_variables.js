function CrearVariables()
{

//Manejo de la variable precio
    var precio = 0,
    resultado;
    resultado = document.getElementById('resultado');
    resultado.innerHTML +=
    "<br> La variable precio vale: " +
    precio + "<br>";

// Cambiamos la variable a tipo Number
    precio = 22.50;
    precio = precio + 10;                   /* 22.50 + 10 = 32.50 */
    resultado.innerHTML +=
          "La variable precio vale: " +
              precio + "<br>" +
          "El doble de precio es: "+
              (precio*2) + "<br>"           /* precio es  = 32.50 || 32.50 * 2 = 65 */

// Cambio el tipo de datos Number --> String
    precio = "Muy caro";
    resultado.innerHTML +=
          "La variable precio vale: " +
              precio + "<br>" +
          "El doble de precio es: "+
              (precio*2) + "<br>"
}
