function tableroNegras() {
    //isNaN Sirve para alertar de un error en caso de que el usuario meta algo equivocado, en este caso una letra u otro caracter especial
    if (isNaN(document.getElementById('pfila1').value ) )
    {
        alert("Sólo se pueden números y del 1 al 8");
        return false;
    }

    if ( isNaN(document.getElementById('pcolumna1').value) )
    {
        alert("Sólo se pueden números y del 1 al 8");
        return false;
    }
        //variables de posicionamineto y llamadas
    var negras,
        pfila1,
        pcolumna1;

    negras = document.getElementById('figurasN').value;              //id del selector de fichas
    pfila1 =  document.getElementById('pfila1').value -1;            //fila y columna, en la que pondremos la ficha seleccionada
    pcolumna1 = document.getElementById('pcolumna1').value -1;       //El -1 sirve para que el usuario pueda introducir correctamente la fila y columna (1-8)
    var areaC,                                                       //Variables para los calculos de las posiciones
        areaF;
    areaC = document.getElementById('pcolumna1').value -1;
    areaF = document.getElementById('pfila1').value -1;    
    var tamanio = 8,                                                //Variables relacionadas con el tablero
        resultado,
        fila,
        columna;

    var crearTablero = new Array ();                         //Array del tablero
        for( var n = 0; n<tamanio; n++)
            crearTablero[n] = new Array;
    resultado = document.getElementById('tablero');           //id que nos mostrara el tablero con la pieza indicada
    resultado.innerHTML = '';                                 //Limpia el tablero antiguo y pone el nuevo modificado

//recorremos la aArray con un for y ponemos los cuadrados blancos en las celdas que no seas divisibles entre 2  y los cuadrados negros en las que lo son
        for(fila=0; fila < tamanio; fila++){                    
            for(columna=0; columna < tamanio; columna++){
                if ( ((fila % 2 != 0) && (columna % 2 == 0)) ||  ((fila % 2 == 0) && (columna % 2 != 0)) )
                {
                    crearTablero[fila][columna] = "<img src='IMG/negro.png'/>";
                }
                else
                {
                    crearTablero[fila][columna] = "<img src='IMG/blanco.png'/>";
                }
            }
        }
//El usuario mete las cordenadas del 1 al 8 pero la array y la fichas van del 0 al 7.
        if (negras == "Rey") //REY
        {
            if((pfila1 == 0) && (pcolumna1 == 0) || (pfila1 == 0) && (pcolumna1 == 7) || (pfila1==0)) //Primera linea del tablero (0) Limite Superior
            {
                crearTablero[areaF +1][pcolumna1] = "<img src='IMG/verde.png'/>";
                crearTablero[areaF+1][areaC+1] ="<img src='IMG/verde.png'/>";   
                crearTablero[areaF+1][areaC-1] ="<img src='IMG/verde.png'/>";
                crearTablero[pfila1][areaC+1]= "<img src='IMG/verde.png'/>";
                crearTablero[pfila1][areaC-1]= "<img src='IMG/verde.png'/>";        
            }
            if((pfila1 == 7) && (pcolumna1 == 0) || (pfila1 == 7) && (pcolumna1 == 7)|| (pfila1==7))  //Ultima linea del tablero (7) Limite Inferior
            {
                crearTablero[areaF -1][pcolumna1] = "<img src='IMG/verde.png'/>";
                crearTablero[areaF-1][areaC-1] ="<img src='IMG/verde.png'/>";   
                crearTablero[areaF-1][areaC+1] ="<img src='IMG/verde.png'/>";
                crearTablero[pfila1][areaC-1]= "<img src='IMG/verde.png'/>";
                crearTablero[pfila1][areaC+1]= "<img src='IMG/verde.png'/>";        
            }
            if((pfila1!=0)&&(pfila1!=7))                                                         //resto de cuadrados
            {
                crearTablero[areaF +1][pcolumna1] = "<img src='IMG/verde.png'/>";
                crearTablero[areaF+1][areaC+1] ="<img src='IMG/verde.png'/>";   
                crearTablero[areaF+1][areaC-1] ="<img src='IMG/verde.png'/>";
                crearTablero[pfila1][areaC+1]= "<img src='IMG/verde.png'/>";
                crearTablero[pfila1][areaC-1]= "<img src='IMG/verde.png'/>";
                crearTablero[areaF -1][pcolumna1] = "<img src='IMG/verde.png'/>";
                crearTablero[areaF-1][areaC-1] ="<img src='IMG/verde.png'/>";   
                crearTablero[areaF-1][areaC+1] ="<img src='IMG/verde.png'/>";
            }
            //Piezas    
            crearTablero[pfila1][pcolumna1] ="<img src='IMG/negras/reyN.png'/>";
        }
        if(negras == "Reina") //Los movimientos de la reina son una copia del alfil y de la torre
        {
            var  inicioF = areaF,      //He creado una variable nueva, le paso lo que vale fila y columna     
                 inicioC = areaC;
                    
            //Arriba
            for(areaF = areaF-1; areaF >= 0; areaF--)
                crearTablero[areaF][pcolumna1] = "<img src='IMG/verde.png'/>";
            //Abajo
            for(areaF = areaF+1; areaF < tamanio; areaF ++)
                crearTablero[areaF][pcolumna1] = "<img src='IMG/verde.png'/>";
            //Izquierda
            for(areaC; areaC >= 0; areaC--)
               crearTablero[pfila1][areaC] = "<img src='IMG/verde.png'/>";
            //Derecha
            for(areaC; areaC < tamanio; areaC++ )
                crearTablero[pfila1][areaC] = "<img src='IMG/verde.png'/>";

            areaC = inicioC;        //Cada vez que haga una diagonal se reinician los valores
            areaF = inicioF;
            
            while( (areaF < tamanio) && (areaC < tamanio) )//Diagonal abajo derecha
            {
                crearTablero[areaF++][areaC++] = "<img src='IMG/verde.png'/>";
            }
            areaC = inicioC;
            areaF = inicioF;
            
            while ( (areaF < tamanio) && (areaC >=0) )      //Diagonal abajo izquierda
            {
                crearTablero[areaF++][areaC--] = "<img src='IMG/verde.png'/>";
            }
            areaC = inicioC;
            areaF = inicioF;
            while( (areaF >= 0) && (areaC >=0) )            //Diagonal arriba izquierda
            {
                crearTablero[areaF--][areaC--] = "<img src='IMG/verde.png'/>";
            }
            areaC = inicioC;
            areaF = inicioF;
            while ( (areaF >=0 ) && (areaC <tamanio) )      //Diagonal arriba derecha
            {
                crearTablero[areaF--][areaC++] = "<img src='IMG/verde.png'/>";   
            }
            //Piezas
            crearTablero[pfila1][pcolumna1] = "<img src='IMG/negras/reinaN.png'/>";
        }
        if(negras == "Peon")
        {   
            if(pfila1 == 7){
                crearTablero[pfila1][pcolumna1] ="<img src='IMG/negras/peonN.png'/>";
            }else{
            crearTablero[areaF+1][pcolumna1] ="<img src='IMG/verde.png'/>";  //Adelante
            crearTablero[areaF+1][areaC+1] ="<img src='IMG/verde.png'/>";   //Diagonal Derecha
            crearTablero[areaF+1][areaC-1] ="<img src='IMG/verde.png'/>";   //Diagonal Izquierda
            }
            //Pieza
            crearTablero[pfila1][pcolumna1] = "<img src='IMG/negras/peonN.png'/>";
        }
        if(negras == "Alfil")
        {
            var  inicioF = areaF,         //He creado una variable nueva, le paso lo que vale fila y columna
                 inicioC = areaC;
            //Se podria haber echo con un for doble, el ehile es por variar y hacer el bucle de otra forma
            while( (areaF < tamanio) && (areaC < tamanio) )//Diagonal abajo derecha
            {
                crearTablero[areaF++][areaC++] = "<img src='IMG/verde.png'/>";
            }
            areaC = inicioC;            //Cada vez que haga una diagonal se reinician los valores
            areaF = inicioF;
            
            while ( (areaF < tamanio) && (areaC >=0) )      //Diagonal abajo izquierda
            {
                crearTablero[areaF++][areaC--] = "<img src='IMG/verde.png'/>";
            }
            areaC = inicioC;
            areaF = inicioF;
            while( (areaF >= 0) && (areaC >=0) )            //Diagonal arriba izquierda
            {
                crearTablero[areaF--][areaC--] = "<img src='IMG/verde.png'/>";
            }
            areaC = inicioC;
            areaF = inicioF;
            while ( (areaF >=0 ) && (areaC <tamanio) )      //Diagonal arriba derecha
            {
                crearTablero[areaF--][areaC++] = "<img src='IMG/verde.png'/>";   
            }
            //piezas
            crearTablero[pfila1][pcolumna1] = "<img src='IMG/negras/alfilN.png'/>";
        }
        if(negras == "Torre")//Es una for que reccorre todas las casilas en linea recta desde la posicion de la figura
        {
            //Arriba
            for(areaF = areaF-1; areaF >= 0; areaF--)
                crearTablero[areaF][pcolumna1] = "<img src='IMG/verde.png'/>";
            //Abajo
            for(areaF = areaF+1; areaF < tamanio; areaF ++)
                crearTablero[areaF][pcolumna1] = "<img src='IMG/verde.png'/>";
            //Izquierda
            for(areaC; areaC >= 0; areaC--)
               crearTablero[pfila1][areaC] = "<img src='IMG/verde.png'/>";
            //Derecha
            for(areaC; areaC < tamanio; areaC++ )
                crearTablero[pfila1][areaC] = "<img src='IMG/verde.png'/>";
            //Pieza            
            crearTablero[pfila1][pcolumna1] = "<img src='IMG/negras/torreN.png'/>";
        }
        //Una vez puesta la ficha y sus movimientos, terminamos con este for, que lo que nos hace
        // es pintar el resto del tablero, sin pintar aquellas celdas en donde ete la figura y sus movimientos
        for(fila=0; fila < tamanio; fila++)
        {
            for(columna=0; columna < tamanio; columna++)
            {
                resultado.innerHTML += crearTablero[fila][columna];
            }
        }
}