function tableroAjedrez() {

	var tamanio = 8,
		resultado,
		fila,
		columna;

//Array que contiene el tablero
	
	var crearTablero = new Array ();
	for( var n = 0; n<tamanio; n++)
		crearTablero[n] = new Array;

	resultado = document.getElementById('tablero');
	resultado.innerHTML = '';

	for(fila=0; fila < tamanio; fila++){
		for(columna=0; columna < tamanio; columna++){
			if ( ((fila % 2 != 0) && (columna % 2 == 0)) ||  ((fila % 2 == 0) && (columna % 2 != 0)) )
			{
				crearTablero[fila][columna] = "<img src='IMG/negro.png'/>";
				resultado.innerHTML += crearTablero[fila][columna];
			}
			else
			{
				crearTablero[fila][columna] = "<img src='IMG/blanco.png'/>";
				resultado.innerHTML += crearTablero[fila][columna];
			}
		}
	}
}
function borrar_tablero()
{
	var limpiar;
	limpiar = document.getElementById('tablero');
	limpiar.innerHTML = '';
}